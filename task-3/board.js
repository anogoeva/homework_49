const size = 8;
for (const i = 0; i < size; i++) {
    if (i % 2 === 0) {
        console.log("  ██".repeat(size / 2));
    } else {
        console.log("██  ".repeat(size / 2));
    }
}
